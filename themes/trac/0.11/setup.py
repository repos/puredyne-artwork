#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

from setuptools import setup

setup(
    name = 'TracPureDyneTheme',
    version = '1.0',
    packages = ['puredynetheme'],
    package_data = { 'puredynetheme': ['templates/*.html', 'htdocs/*.png', 'htdocs/*.css'] },

    author = 'GOTO10',
    author_email = 'puredyne-team@goto10.org',
    description = 'Trac Theme for the pure:dyne website',
    license = 'GPL',
    keywords = 'trac plugin theme',
    url = 'http://puredyne.goto10.org',
    classifiers = [
        'Framework :: Trac',
    ],
    
    install_requires = ['Trac', 'TracThemeEngine>=2.0'],

    entry_points = {
        'trac.plugins': [
            'puredynetheme.theme = puredynetheme.theme',
        ]
    },
)
